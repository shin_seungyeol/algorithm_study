# 옆에 있는 원소와 비교하여, 더 작은 값을 앞으로, 더 큰 값을 뒤로 보내는 작업을 반복하는 정렬
# 효율성이 가장 안좋은 정렬법
# 뒤부터 정렬이 되는 특성이 있다.

def bubble_sort (arr) :
    # i : 정렬이 완료된 원소의 개수
    # j : 비교할 원소의 index
    for i in range(0, len(arr)) : 
        for j in range(0, len(arr)-i-1) :
            if arr[j] > arr[j+1] :
                arr[j+1], arr[j] = arr[j], arr[j+1]

if __name__ == '__main__' :
    arr = [1,3,2,5,7,0,1,2,5,23,321]
    bubble_sort(arr)
    print(arr)