# 퀵 정렬은 대표적인 '분할 정복' 알고리즘으로 평균 속도가 O(NlogN)이다. 
# 최악의 경우에는 O(n^2)
# 피벗(Pivot) 값을 기준으로 왼족과 오른쪽으로 나누는 작업을 반복적으로 수행한다.

def quick_sort (start, end, arr) :
    # base_case 
    if start >= end :
        return 

    pivot = start
    left = start + 1
    right = end
    
    while left <= right :
        if arr[left] <= arr[pivot] :
            left += 1

        if arr[right] > arr[pivot] and right > start :
            right -= 1
        
        if left > right :
            break

        if arr[left] > arr[pivot] and arr[right] <= arr[pivot] :
            arr[left], arr[right] = arr[right], arr[left]
            left += 1
            right -= 1 

    arr[pivot], arr[right] = arr[right], arr[pivot]

    quick_sort(start, right-1, arr)
    quick_sort(right+1, end, arr)

if __name__ == '__main__' :
    arr = [1,3,2,5,7,0,1,2,5,23,32, 11, 11412, 34124]
    quick_sort(0, len(arr)-1, arr)
    print(arr)