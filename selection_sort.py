# 가장 작은 원소를 선택하여, 가장 앞에 놓는 정렬법
def selection_sort (arr) :
    for i in range(len(arr)) : 
        min_idx = i
        for j in range (i, len(arr)) : 
            if arr[min_idx] > arr[j] :
                min_idx = j
        # swap        
        arr[min_idx], arr[i] = arr[i], arr[min_idx] 

if __name__ == '__main__' :
    arr = [1,3,2,5,7,0,1,2,5,23,321]
    selection_sort(arr)
    print(arr)
                 
         