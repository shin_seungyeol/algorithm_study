# 각 숫자를 적잘한 위치에 삽입하는 정렬법
# 필요할 때만 위치를 바꾼다 => 버블정렬과 선택 정렬보다 빠를 수 있다. 
# 앞에 있는 원소들이 정렬되 있다고 가정을 한다. 
# 정렬이 거의 되어 있는 경우 O(n)으로 가장 빠른 정렬 알고리즘이 될 수 있다. 

def insertion_sort (arr) :
    for i in range(1, len(arr)) :
        for j in range(i-1, -1, -1) :
            if arr[j] > arr[j+1] :
                arr[j], arr[j+1] = arr[j+1], arr[j]
            else :
                break
       
         

if __name__ == '__main__' :
    arr = [1,3,2,5,7,0,1,2,5,23,321]
    insertion_sort(arr)
    print(arr)
                 