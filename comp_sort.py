import time, copy, random
from quick_sort import quick_sort
from bubble_sort import bubble_sort
from insertion_sort import insertion_sort
from selection_sort import selection_sort

if __name__ == "__main__" :
    
    arr_size = 10000

    arr = []
    for i in range(arr_size) :
        arr.append(random.randrange(1, 10000))
    
    selection_temp = copy.copy(arr)
    start = time.time()
    selection_sort(selection_temp)
    end = time.time()
    print("배열 사이즈 " + str(arr_size) + "일 때 SELECTION SORT 측정 시간 " + str(end-start))

    bubble_temp = copy.copy(arr)
    start = time.time()
    bubble_sort(bubble_temp)
    end = time.time()
    print("배열 사이즈 " + str(arr_size) + "일 때 BUBBLE SORT 측정 시간 " + str(end-start))

    insertion_temp = copy.copy(arr)
    start = time.time()
    insertion_sort(insertion_temp)
    end = time.time()
    print("배열 사이즈 " + str(arr_size) + "일 때 INSERTION SORT 측정 시간 " + str(end-start))

    quick_temp = copy.copy(arr)
    start = time.time()
    quick_sort(0, len(quick_temp)-1, quick_temp)
    end = time.time()
    print("배열 사이즈 " + str(arr_size) + "일 때 QUICK SORT 측정 시간 " + str(end-start))

    print(selection_temp == bubble_temp == insertion_temp == quick_temp)




